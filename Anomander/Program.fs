﻿// Learn more about F# at http://fsharp.net
// See the 'F# Tutorial' project for more help.

open System.Xml
open System.Xml.Schema

[<EntryPoint>]
let main argv = 
    printfn "%A" argv
    let next = new RptLoader.Rpt(argv.[0])


    let resultStuff = CrystalToXML.ReportDocumentToXML(next)
    System.Console.WriteLine resultStuff


    System.Console.ReadKey() |> ignore
    0 // return an integer exit code