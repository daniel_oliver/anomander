﻿module CrystalToXML

open CrystalDecisions.CrystalReports.Engine
open System.Xml.Linq

let ToXMLArray(seqStart : seq<'a>, sortFun, toXMLFun) = 
  seqStart
  |> Seq.sortBy sortFun
  |> Seq.map toXMLFun
  |> Seq.toArray<XElement>
//############################################################################
//Formula Fields
let FormulaFieldToXML(arg : FormulaFieldDefinition) = 
  new XElement(XName.Get "Field", 
    [| 
      new XElement(XName.Get "Name", arg.Name)
      new XElement(XName.Get "Text", arg.Text) 
    |]
  )

let FormulaFieldList(arg: RptLoader.Rpt) =
  new XElement(XName.Get "FormulaFields",
    ToXMLArray(arg.FormulaFields, (fun M -> M.Name), FormulaFieldToXML)
  )
//############################################################################
//Group Name Fields
let GroupNameFieldToXML(arg: GroupNameFieldDefinition) =
  new XElement(XName.Get "Field",
    [|
      new XElement(XName.Get "Name", arg.GroupNameFieldName)
    |]
  )

let GroupNameFieldList(arg: RptLoader.Rpt) =
  new XElement(XName.Get "GroupNameFields",
    ToXMLArray(arg.GroupNameFields, (fun M -> M.GroupNameFieldName), GroupNameFieldToXML)
  )
//############################################################################
//Parameter Fields
let ParameterFieldToXML(arg: ParameterFieldDefinition) =
  new XElement(XName.Get "Field",
    [|
      new XElement(XName.Get "Name", arg.Name)
      //TODO FILL OUT MORE INFO
    |]
  )

let ParameterFieldList(arg: RptLoader.Rpt) =
  new XElement(XName.Get "ParameterFields",
    ToXMLArray(arg.ParameterFields, (fun M -> M.Name), ParameterFieldToXML)
  )
//############################################################################
//Running Total Fields
let RunningTotalToXML(arg: RunningTotalFieldDefinition) =
  new XElement(XName.Get "Field",
    [|
      new XElement(XName.Get "Name", arg.Name)
      new XElement(XName.Get "Summary", arg.SummarizedField.Name)
      //TODO FILL OUT MORE INFO
    |]
  )

let RunningTotalList(arg: RptLoader.Rpt) =
  new XElement(XName.Get "RunningTotalFields",
    ToXMLArray(arg.RunningTotalFields, (fun M -> M.Name), RunningTotalToXML)
  )
//############################################################################
//Summary Fields
let SummaryFieldToXML(arg: SummaryFieldDefinition) =
  new XElement(XName.Get "Field",
    [|
      new XElement(XName.Get "Name", arg.Name)
      new XElement(XName.Get "Summary", arg.SummarizedField.Name)
      //TODO FILL OUT MORE INFO
    |]
  )

let SummaryFieldList(arg: RptLoader.Rpt) =
  new XElement(XName.Get "SummaryFields",
    ToXMLArray(arg.SummaryFields, (fun M -> M.Name), SummaryFieldToXML)
  )
//############################################################################
//Sub Reports
let SubReportList(arg : RptLoader.Rpt, rptToFun) = 
  new XElement(XName.Get "SubReports", 
               ToXMLArray(arg.SubReports, (fun M -> M.ReportName), rptToFun))
//############################################################################
//Report
let rec ReportDocumentToXML(arg : RptLoader.Rpt) = 
  new XElement(XName.Get "Report", 
    [| 
      new XElement(XName.Get "Name", arg.ReportName)
      FormulaFieldList(arg)
      GroupNameFieldList(arg)
      ParameterFieldList(arg)
      RunningTotalList(arg)
      SummaryFieldList(arg)
      SubReportList( arg, ReportDocumentToXML)
    |]
  )