﻿module RptLoader

open CrystalDecisions.CrystalReports.Engine

//Provides Seq access to all the datadefinition collections
type Rpt(arg : ReportDocument) = 
  let rptDocument = arg
  
  //Loads a report given a path+filename
  new(fileName : string) = 
    let rptDocToLoad = new ReportDocument()
    rptDocToLoad.Load(fileName, CrystalDecisions.Shared.OpenReportMethod.OpenReportByTempCopy)
    rptDocToLoad.Refresh()
    Rpt(rptDocToLoad)
  
  member this.FormulaFields = rptDocument.DataDefinition.FormulaFields |> Seq.cast<FormulaFieldDefinition>
  member this.GroupNameFields = rptDocument.DataDefinition.GroupNameFields |> Seq.cast<GroupNameFieldDefinition>
  member this.ParameterFields = rptDocument.DataDefinition.ParameterFields |> Seq.cast<ParameterFieldDefinition>
  member this.ReportName = rptDocument.Name
  member this.RunningTotalFields = rptDocument.DataDefinition.RunningTotalFields |> Seq.cast<RunningTotalFieldDefinition>
  member this.SubReports = 
    if rptDocument.IsSubreport then Seq.empty
    else rptDocument.Subreports |> Seq.cast<ReportDocument> |> Seq.map (fun M -> new Rpt(M))
  member this.SummaryFields = rptDocument.DataDefinition.SummaryFields |> Seq.cast<SummaryFieldDefinition>
  